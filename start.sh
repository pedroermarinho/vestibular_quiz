#!/usr/bin/env bash
until
# sudo python3 run.py & sudo python3 telegram.py;
sudo python3 telegram.py;
 do
    echo "Whatsapp bot crashed with code $?.  Respawning.." >&2
    sleep 1
done
