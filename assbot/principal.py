# -*- coding:utf-8  -*-
import os
from chatterbot import ChatBot
import shutil
import logging
from assbot.Palavras_Chave import Palavra_Chave
from assbot.Comandos import Comando


class Main(object):  # class 3
    palavra_chaves = Palavra_Chave()
    Comando = Comando()

    # Enable info level logging
    logging.basicConfig(level=logging.INFO)

    CalculadoraBot = ChatBot('CalculadoraBot',
                             logic_adapters=[
                                 "chatterbot.logic.MathematicalEvaluation"
                             ],
                             input_adapter="chatterbot.input.VariableInputTypeAdapter",
                             output_adapter="chatterbot.output.OutputAdapter")

   

    print('\nOlá, Bem Vindo ao nosso bot :)\n')

    def mensagem_bot_pergunta(self, text=None):  # função que ira tratar as mensagens
        if text is None:  # caso a função nao recebar nenhum parametro ele ira receber o parametro do terminal
            return input('\nDigite algo:')
        else:
            return str(text)  # tranforma a variavel text em string

    result_mensagem_bot_resposta = None

    def mensagem_bot_resposta(self, cmd):  # função responsavel por gera uma reposta para a pergunta
        result = self.Comando.executar_cmd(
            self.Comando.comando(cmd))  # verifica se é um comando e se for retonara o resultado
        if result is None:  # verifica se é algum comado
            result = self.palavra_chaves.pesquisa_na_wikipedia(
                cmd)  # verifica se é uma pesquisa , se for <retornara o resultado da pesquisa

            if result is None:  # verifica se é algum comado
                result = self.palavra_chaves.pesquisa_definicao(
                    cmd)  # verifica se é uma pesquisa , se for <retornara o resultado da pesquisa

                if result is None:  # verifica se é algum comado
                    result = self.palavra_chaves.pesquisa_no_google(
                        cmd)  # verifica se é uma pesquisa , se for <retornara o resultado da pesquisa

                    if result is None:
                        calc = cmd.replace('Quanto é',
                                           '')  # troca calc por What is pois calculadora sor recee comandos em ingles
                        try:

                            for letras in calc:
                                if letras.isalpha():
                                    calc = None
                                    break

                            if calc is not None:
                                result = self.CalculadoraBot.get_response(calc)  # função da chamda da calculadora
                                if result.confidence < 0.99:  # se a confiança da calcualdora for aixa ele retorna none
                                    result = None
                                elif result == "e = 2.718281":
                                    result = None
                            else:
                                result = None

                        except:  # se der errp
                            result = None

                        if result is None:
                            try:
                                arq = open('assbot/palavras_6/palavras.txt', 'a')
                                arq.writelines('\n\n' + str(cmd))  # gravar palavras desconhecida
                                arq.close()
                            except:
                                print('\nErro: palavras\n')

                            
                            result = self.palavra_chaves.wikipedia(cmd)

                        
        return result  # retorna ao resultado

# ----------------------------------------------------------------------------------------------------------------------
