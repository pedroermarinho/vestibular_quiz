import wikipedia
import random

from assbot.questao import quest
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from googlesearch import search
from urllib.request import urlopen
from bs4 import BeautifulSoup



class Palavra_Chave():
    wikipedia.set_lang('pt')  # determina que todas as pesquisas da wikipedia sejam em portugues
    palavras_chaves_wikipedia = []  # palavras chaves para pesquisas na wikipedia
    palavras_chaves_google = []  # palavras chaves para pesquisas na wikipedia
    palavras_chaves_definicao = []  # palavras chaves para pesquisas na wikipedia

    comandos_wiki = None
    try:
        comandos_wiki = open('assbot/Palavras_chaves/wikipedia.txt',
                        'r').readlines()  # abrindo e lendo o arquivo onde esta o comando
    except():
        print('\nerro ao tenta arir os arquivos comandos\n')
        comandos_wiki = None
    if comandos_wiki is not None:
        n = 0
        for comandow in comandos_wiki:  # percorendo todos os comandos
            comandow = comandow.replace('\n', '')  # deletando as quebras de linha
            palavras_chaves_wikipedia.insert(n, comandow)  # colocados dentro da lista as mensagens e os comandos
            n = n + 1


    comandos_google = None
    try:
        comandos_google = open('assbot/Palavras_chaves/google.txt',
                        'r').readlines()  # abrindo e lendo o arquivo onde esta o comando
    except():
        print('\nerro ao tenta arir os arquivos comandos\n')
        comandos_google = None
    if comandos_google is not None:
        n = 0
        for comandog in comandos_google:  # percorendo todos os comandos
            comandog = comandog.replace('\n', '')  # deletando as quebras de linha
            palavras_chaves_google.insert(n, comandog)  # colocados dentro da lista as mensagens e os comandos
            n = n + 1

    comandos_definicao = None
    try:
        comandos_definicao = open('assbot/Palavras_chaves/definicao.txt',
                        'r').readlines()  # abrindo e lendo o arquivo onde esta o comando
    except():
        print('\nerro ao tenta arir os arquivos comandos\n')
        comandos_definicao = None
    if comandos_definicao is not None:
        n = 0
        for comandod in comandos_definicao:  # percorendo todos os comandos
            comandod = comandod.replace('\n', '')  # deletando as quebras de linha
            palavras_chaves_definicao.insert(n, comandod)  # colocados dentro da lista as mensagens e os comandos
            n = n + 1



    def pesquisa_na_wikipedia(self, texto):  # função para pesquisa na wikipedia
        print("wiki")

        result = None  # resultado

        for chave in self.palavras_chaves_wikipedia:  # percorrer as palavras chaves
            if texto.lower().startswith(chave.lower()):  # verifica se texto começa com alguma palavra chave
                result = texto.lower().replace(chave.lower(), '')  # texto atual menos a chave utilizada
        if result is not None:  # vrefica se result não é nulo
            try:
                print('pesquisando')
                return wikipedia.summary(wikipedia.search(result)[0], sentences=2)  # retornara ao primeiro resultado
            except:
                print('\nerro função: def pesquisa_na_wikipedia\n')
                return 'Não foi possivel fazer a pesquisa'


    def pesquisa_no_google(self, texto):  # função para pesquisa na wikipedia



        result = None  # resultado

        for chave in self.palavras_chaves_google:  # percorrer as palavras chaves
            if texto.lower().startswith(chave.lower()):  # verifica se texto começa com alguma palavra chave
                result = texto.lower().replace(chave.lower(), '')  # texto atual menos a chave utilizada
        if result is not None:  # vrefica se result não é nulo
            try:
                print('pesquisando')
                for url in search(texto,stop=1):
                    result=url
                    break
                html = urlopen(url)
                res = BeautifulSoup(html.read(), "html5lib")
                tags = res.findAll("p")
                result = result+"\n"+tags.__getitem__(0).getText()
                return result  # retornara ao primeiro resultado
            except:
                print('\nerro função: def pesquisa_na_wikipedia\n')
                return 'Não foi possivel fazer a pesquisa'


    def pesquisa_definicao(self, texto):  # função para pesquisa na wikipedia
        print("definição")

        result = None  # resultado
        print(self.palavras_chaves_definicao)
        for chave in self.palavras_chaves_definicao:  # percorrer as palavras chaves
            if texto.lower().startswith(chave.lower()):  # verifica se texto começa com alguma palavra chave
                result = texto.lower().replace(chave.lower(), '')  # texto atual menos a chave utilizada
                result = result.replace(" ", '')
        if result is not None:  # vrefica se result não é nulo
            try:
                print('pesquisando definição->' + result)
                html = urlopen("https://www.significados.com.br/?s="+str(result))
                res = BeautifulSoup(html.read(), "html5lib")
                tags = res.findAll("p")
                result = tags.__getitem__(1).getText()
                return result
            except:
                print('\npesquisa_definicaon')
                return 'Não foi possivel fazer a pesquisa'

    def wikipedia(self, text):
        try:
            print('pesquisando')
            result = wikipedia.summary(wikipedia.search(text)[0], sentences=2)  # retornara ao primeiro resultado
        except:
            print('\nerro função: def wikipedia\n')
            result = None
        return result

    def definicao(self, text):
        try:
            print('pesquisando definição->'+text)
            html = urlopen("https://www.significados.com.br/?s=" +text)
            res = BeautifulSoup(html.read(), "html5lib")
            tags = res.findAll("p")
            result = tags.__getitem__(1).getText()
            return result
        except:
            print('\nerro função: definicao\n')
            result = None
        return result
    # ------------------------------------------------------------------------------------------------------------------
    Piadas_dic = []  # dicionario com as piadas
    piadas = None
    try:
        piadas = open('assbot/ass_diversos/piadas.txt', 'r').readlines()  # ler o arquivo onde estão as piadas
    except():
        piadas = None
    if piadas is not None:
        cont = 0
        for piada in piadas:  # percorre as as piadas
            piada = piada.replace('\n', '')  # ignora as quebras de linhas
            Piadas_dic.insert(cont, piada)  # acionar as piadas no dicionario
            cont = cont + 1
    print("piadas->"+str(Piadas_dic.__len__()))
    # ------------------------------------------------------------------------------------------------------------------
    Charadas_dic = []  # dicionario com as Charadas
    Charadas = None
    try:
        Charadas = open('assbot/ass_diversos/Charadas.txt', 'r').readlines()  # ler o arquivo onde estão as piadas
    except():
        Charadas = None
    if Charadas is not None:
        cont = 0
        for Charada in Charadas:  # percorre as as piadas
            Charada = Charada.replace('\n', '')  # ignora as quebras de linhas
            Charadas_dic.insert(cont, Charada)  # acionar as Charadas no dicionario
            cont = cont + 1
    print(Charadas_dic.__len__())
    # ------------------------------------------------------------------------------------------------------------------
    Citacoes_dic = []  # dicionario com as Citacao
    Citacoes = None
    try:
        Citacoes = open('assbot/ass_diversos/Citacao.txt', 'r').readlines()  # ler o arquivo onde estão as piadas
    except():
        Citacoes = None
    if Citacoes is not None:
        cont = 0
        for Citacao in Citacoes:  # percorre as as piadas
            Citacao = Citacao.replace('\n', '')  # ignora as quebras de linhas
            Citacoes_dic.insert(cont, piada)  # acionar as Citacao no dicionario
            cont = cont + 1
    print("Citacoes"+str(Citacoes_dic.__len__()))

    # ------------------------------------------------------------------------------------------------------------------
    curiosidades_dic = []  # dicionario com as curiosidade
    curiosidades = None
    try:
        curiosidades = open('assbot/ass_diversos/curiosidade.txt', 'r').readlines()  # ler o arquivo onde estão as curiosidade
    except():
        curiosidades = None
    if curiosidades is not None:
        cont = 0
        for curiosidade in curiosidades:  # percorre as as curiosidades
            curiosidade = curiosidade.replace('\n', '')  # ignora as quebras de linhas
            curiosidades_dic.insert(cont, curiosidade)  # acionar as curiosidade no dicionario
            cont = cont + 1
    print("curiosidades"+str(curiosidades_dic.__len__()))

    # ------------------------------------------------------------------------------------------------------------------

    def Numero_aleatorio_piada(self):
        return random.randint(0, self.Piadas_dic.__len__() - 1)

    def Numero_aleatorio_Charada(self):
        return random.randint(0, self.Charadas_dic.__len__() - 1)

    def Numero_aleatorio_citacao(self):
        return random.randint(0, self.Citacoes_dic.__len__() - 1)

    def Numero_aleatorio_curiosidade(self):
        return random.randint(0, self.curiosidades_dic.__len__() - 1)

    def piada(self):
        try:
            return self.Piadas_dic[self.Numero_aleatorio_piada()]
        except:
            return None
    def Charada(self):
        try:
            return self.Piadas_dic[self.Numero_aleatorio_Charada()]
        except:
            return None
    def citacao(self):
        try:
            return self.Citacoes_dic[self.Numero_aleatorio_citacao()]
        except:
            return None
    def curiosidade(self):
        try:
            return self.curiosidades_dic[self.Numero_aleatorio_curiosidade()]
        except:
            return None


    #     palavras_chaves_calc = ['calcule', 'Calcule', 'quanto é', 'Quanto é', 'calc', 'Calc']
  
    dicionario_quest = []
    cont = 0
    quests = quest()

    arquivo = open('assbot/quest/enem.txt', 'r').readlines()
    for comando in arquivo:  # percorendo todos os comandos
        comando = comando.replace('\n', '')  # deletando as quebras de linha
        parts = comando.split('||')  # separando mensaguem de comando // dicionario de comandos
        quests.set_pergunta(parts[0])
        quests.set_questA(parts[1])
        quests.set_questB(parts[2])
        quests.set_questC(parts[3])
        quests.set_questD(parts[4])
        quests.set_questE(parts[5])
        quests.set_resposta(parts[6])

        dicionario_quest.insert(cont, quests.result())
        cont = cont + 1


    def questao_enem(self):
        cont=random.randint(0, self.dicionario_quest.__len__() - 1)
        return self.dicionario_quest[cont]