# modules/hi_module.py

from app.mac import mac, signals
from assbot.principal import Main
from modules.Vestibular import questao
from assbot.Comandos import Comando


# from modules.q_academico_ifam.q_academico import notas
numbers = ["1⃣", "2⃣", "3⃣", "4⃣", "5⃣"]
main = Main()
cmds= Comando()
@signals.message_received.connect
def handle(message):
    name = message.who_name
    
    
    
    pergunta = main.mensagem_bot_pergunta(message.text)  # função para transformar o texto e verificar sua entrada
    

    if message.command == "v":
        mac.send_message(str(questao.quest(name)), message.conversation)
    elif "cmd_quiz" == cmds.comando(pergunta):
        mac.send_message(str(questao.quest(name)), message.conversation)
    elif pergunta in numbers:
        mac.send_message(str(questao.resposta(pergunta,name)), message.conversation)
    
    else:

        resposta = main.mensagem_bot_resposta(pergunta)  # função que captura a pegunta e gera uma resposta
        # print(resposta)# mostra na tela a resposta
        try:
            mac.send_message(str(resposta).format(str(name)), message.conversation)
        except:
            # mac.send_message("Avisamos que a Dominik  ainda está em fase de teste , a cada dia estará mais inteligente \n\n"+str(resposta), message.conversation)
            mac.send_message(str(resposta), message.conversation)
            # Can also send media
            #mac.send_image("path/to/image.png", message.conversation)
            #mac.send_video("path/to/video.mp4", message.conversation)
